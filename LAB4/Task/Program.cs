﻿using System;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.UTF8;
        bool exit = false;
        while (!exit)
        {
            exit = SupportMenu();
        }
    }

    static bool SupportMenu()
    {
        Console.WriteLine("Ласкаво просимо до системи підтримки користувачів.");
        Console.WriteLine("Будь ласка, виберіть один з наступних варіантів:");
        Console.WriteLine("1. Технічна підтримка");
        Console.WriteLine("2. Підтримка рахунків");
        Console.WriteLine("3. Загальні питання");
        Console.WriteLine("4. Вихід");

        string choice = Console.ReadLine();

        switch (choice)
        {
            case "1":
                return TechnicalSupport();
            case "2":
                return BillingSupport();
            case "3":
                return GeneralQuestions();
            case "4":
                Console.WriteLine("Дякуємо за користування нашою системою підтримки.");
                return true;
            default:
                Console.WriteLine("Невірний вибір, спробуйте ще раз.");
                return false;
        }
    }

    static bool TechnicalSupport()
    {
        Console.WriteLine("Ви обрали Технічну підтримку.");
        Console.WriteLine("Будь ласка, виберіть один з наступних варіантів:");
        Console.WriteLine("1. Проблеми з підключенням до Інтернету");
        Console.WriteLine("2. Проблеми з програмним забезпеченням");
        Console.WriteLine("3. Повернутися до головного меню");

        string choice = Console.ReadLine();

        switch (choice)
        {
            case "1":
                Console.WriteLine("Ви обрали Проблеми з підключенням до Інтернету. Ми перенаправимо вас до відповідного спеціаліста.");
                return true;
            case "2":
                Console.WriteLine("Ви обрали Проблеми з програмним забезпеченням. Ми перенаправимо вас до відповідного спеціаліста.");
                return true;
            case "3":
                return false;
            default:
                Console.WriteLine("Невірний вибір, спробуйте ще раз.");
                return false;
        }
    }

    static bool BillingSupport()
    {
        Console.WriteLine("Ви обрали Підтримку рахунків.");
        Console.WriteLine("Будь ласка, виберіть один з наступних варіантів:");
        Console.WriteLine("1. Перевірка рахунку");
        Console.WriteLine("2. Скарги на рахунок");
        Console.WriteLine("3. Повернутися до головного меню");

        string choice = Console.ReadLine();

        switch (choice)
        {
            case "1":
                Console.WriteLine("Ви обрали Перевірка рахунку. Ми перенаправимо вас до відповідного спеціаліста.");
                return true;
            case "2":
                Console.WriteLine("Ви обрали Скарги на рахунок. Ми перенаправимо вас до відповідного спеціаліста.");
                return true;
            case "3":
                return false;
            default:
                Console.WriteLine("Невірний вибір, спробуйте ще раз.");
                return false;
        }
    }

    static bool GeneralQuestions()
    {
        Console.WriteLine("Ви обрали Загальні питання.");
        Console.WriteLine("Будь ласка, виберіть один з наступних варіантів:");
        Console.WriteLine("1. Інформація про послуги");
        Console.WriteLine("2. Зворотній зв'язок");
        Console.WriteLine("3. Повернутися до головного меню");

        string choice = Console.ReadLine();

        switch (choice)
        {
            case "1":
                Console.WriteLine("Ви обрали Інформація про послуги. Ми перенаправимо вас до відповідного спеціаліста.");
                return true;
            case "2":
                Console.WriteLine("Ви обрали Зворотній зв'язок. Ми перенаправимо вас до відповідного спеціаліста.");
                return true;
            case "3":
                return false;
            default:
                Console.WriteLine("Невірний вибір, спробуйте ще раз.");
                return false;
        }
    }
}
