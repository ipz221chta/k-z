﻿using System;
using System.Collections.Generic;
using System.Text;

class TextDocument
{
    public string Content { get; set; }

    public TextDocument(string content)
    {
        Content = content;
    }

    public TextDocumentMemento CreateMemento()
    {
        return new TextDocumentMemento(Content);
    }

    public void RestoreMemento(TextDocumentMemento memento)
    {
        Content = memento.Content;
    }
}

class TextDocumentMemento
{
    public string Content { get; }

    public TextDocumentMemento(string content)
    {
        Content = content;
    }
}

class TextEditor
{
    private TextDocument _document;
    private Stack<TextDocumentMemento> _history;

    public TextEditor(string initialContent)
    {
        _document = new TextDocument(initialContent);
        _history = new Stack<TextDocumentMemento>();
    }

    public void Edit(string newContent)
    {
        SaveState();
        _document.Content = newContent;
    }

    public void SaveState()
    {
        _history.Push(_document.CreateMemento());
    }

    public void Undo()
    {
        if (_history.Count > 0)
        {
            var memento = _history.Pop();
            _document.RestoreMemento(memento);
        }
        else
        {
            Console.WriteLine("Немає станів для відновлення.");
        }
    }

    public void ShowContent()
    {
        Console.WriteLine("Поточний вміст документа: " + _document.Content);
    }
}

class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.UTF8;
        TextEditor editor = new TextEditor("Початковий текст");
        bool exit = false;

        while (!exit)
        {
            Console.Clear();
            editor.ShowContent();
            Console.WriteLine("Виберіть дію:");
            Console.WriteLine("1. Редагувати текст");
            Console.WriteLine("2. Відмінити останню зміну");
            Console.WriteLine("3. Вийти");

            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    Console.WriteLine("Введіть новий текст:");
                    string newText = Console.ReadLine();
                    editor.Edit(newText);
                    break;
                case "2":
                    editor.Undo();
                    break;
                case "3":
                    exit = true;
                    break;
                default:
                    Console.WriteLine("Невірний вибір, спробуйте ще раз.");
                    break;
            }
        }
    }
}
