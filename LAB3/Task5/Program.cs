﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

// Базовий клас для всіх вузлів LightHTML
abstract class LightNode
{
    public abstract string OuterHTML { get; }
    public abstract string InnerHTML { get; }
}

// Клас для текстових вузлів
class LightTextNode : LightNode
{
    public string Text { get; private set; }

    public LightTextNode(string text)
    {
        Text = text;
    }

    public override string OuterHTML => InnerHTML;

    public override string InnerHTML => Text;
}

// Типи відображення для елементів
enum DisplayType
{
    Block,
    Inline
}

// Типи закриваючих тегів
enum ClosingType
{
    SelfClosing,
    Normal
}

// Делегат для обробників подій
public delegate void EventListener();

// Клас для елементів
class LightElementNode : LightNode
{
    public string TagName { get; private set; }
    public DisplayType Display { get; private set; }
    public ClosingType Closing { get; private set; }
    public List<string> CssClasses { get; private set; }
    public List<LightNode> Children { get; private set; }
    private Dictionary<string, EventListener> eventListeners;

    public LightElementNode(string tagName, DisplayType display, ClosingType closing)
    {
        TagName = tagName;
        Display = display;
        Closing = closing;
        CssClasses = new List<string>();
        Children = new List<LightNode>();
        eventListeners = new Dictionary<string, EventListener>();
    }

    public void AddClass(string cssClass)
    {
        CssClasses.Add(cssClass);
    }

    public void AddChild(LightNode child)
    {
        Children.Add(child);
    }

    public void AddEventListener(string eventType, EventListener listener)
    {
        if (eventListeners.ContainsKey(eventType))
        {
            eventListeners[eventType] += listener;
        }
        else
        {
            eventListeners[eventType] = listener;
        }
    }

    public void TriggerEvent(string eventType)
    {
        if (eventListeners.ContainsKey(eventType) && eventListeners[eventType] != null)
        {
            eventListeners[eventType].Invoke();
        }
    }

    public override string OuterHTML
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"<{TagName}");

            if (CssClasses.Count > 0)
            {
                sb.Append(" class=\"");
                sb.Append(string.Join(" ", CssClasses));
                sb.Append("\"");
            }

            if (Closing == ClosingType.SelfClosing)
            {
                sb.Append(" />");
            }
            else
            {
                sb.Append(">");
                sb.Append(InnerHTML);
                sb.Append($"</{TagName}>");
            }

            return sb.ToString();
        }
    }

    public override string InnerHTML
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            foreach (var child in Children)
            {
                sb.Append(child.OuterHTML);
            }
            return sb.ToString();
        }
    }
}

// Клас для реалізації патерну легковаговика
class LightElementFlyweight
{
    private Dictionary<string, LightElementNode> elements;

    public LightElementFlyweight()
    {
        elements = new Dictionary<string, LightElementNode>();
    }

    public LightElementNode GetElement(string tagName, DisplayType display, ClosingType closing)
    {
        string key = $"{tagName}_{display}_{closing}";
        if (!elements.ContainsKey(key))
        {
            elements[key] = new LightElementNode(tagName, display, closing);
        }
        return elements[key];
    }
}

// Інтерфейс для завантаження зображень
public interface IImageLoaderStrategy
{
    Task<string> LoadImageAsync(string path);
}

// Реалізація стратегії для завантаження зображень з файлової системи
public class FileSystemImageLoader : IImageLoaderStrategy
{
    public Task<string> LoadImageAsync(string path)
    {
        // Симуляція завантаження зображення з файлової системи
        return Task.FromResult($"Завантажено зображення з файлової системи: {path}");
    }
}

// Реалізація стратегії для завантаження зображень з мережі
public class NetworkImageLoader : IImageLoaderStrategy
{
    public async Task<string> LoadImageAsync(string path)
    {
        using (HttpClient client = new HttpClient())
        {
            // Симуляція завантаження зображення з мережі
            string content = await client.GetStringAsync(path);
            return $"Завантажено зображення з мережі: {path}";
        }
    }
}

// Клас для елементів зображень
public class ImageElement
{
    private IImageLoaderStrategy imageLoaderStrategy;

    public ImageElement(IImageLoaderStrategy loaderStrategy)
    {
        imageLoaderStrategy = loaderStrategy;
    }

    public async Task<string> LoadImage(string path)
    {
        return await imageLoaderStrategy.LoadImageAsync(path);
    }
}

class Program
{
    static async Task Main(string[] args)
    {
        Console.OutputEncoding = Encoding.UTF8;
        Console.WriteLine("Виберіть завдання:\n1: Створення списку з подіями\n2: Завантаження книги та перетворення в HTML\n3: Завантаження зображення");
        int choice = int.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                CreateListWithEvents();
                break;
            case 2:
                await Task2();
                break;
            case 3:
                await LoadImageExample();
                break;
            default:
                Console.WriteLine("Невідоме завдання");
                break;
        }
    }

    private static void CreateListWithEvents()
    {
        // Створення упорядкованого списку <ol>
        LightElementNode ol = new LightElementNode("ol", DisplayType.Block, ClosingType.Normal);
        ol.AddClass("ordered-list");

        // Додавання елементів списку <li>
        LightElementNode li1 = new LightElementNode("li", DisplayType.Block, ClosingType.Normal);
        li1.AddChild(new LightTextNode("Перший елемент списку"));
        li1.AddEventListener("click", () => Console.WriteLine("Клік по першому елементу"));
        ol.AddChild(li1);

        LightElementNode li2 = new LightElementNode("li", DisplayType.Block, ClosingType.Normal);
        li2.AddChild(new LightTextNode("Другий елемент списку"));
        li2.AddEventListener("click", () => Console.WriteLine("Клік по другому елементу"));
        ol.AddChild(li2);

        LightElementNode li3 = new LightElementNode("li", DisplayType.Block, ClosingType.Normal);
        li3.AddChild(new LightTextNode("Третій елемент списку"));
        li3.AddEventListener("mouseover", () => Console.WriteLine("Навели курсор на третій елемент"));
        ol.AddChild(li3);

        // Вивід результату
        Console.WriteLine("OuterHTML:");
        Console.WriteLine(ol.OuterHTML);
        Console.WriteLine("\nInnerHTML:");
        Console.WriteLine(ol.InnerHTML);

        // Тестування подій
        li1.TriggerEvent("click");
        li2.TriggerEvent("click");
        li3.TriggerEvent("mouseover");
    }

    private static async Task Task2()
    {
        string url = "https://www.gutenberg.org/cache/epub/1513/pg1513.txt";

        string[] lines = await DownloadBookAsync(url);

        LightElementFlyweight flyweight = new LightElementFlyweight();

        LightElementNode document = new LightElementNode("html", DisplayType.Block, ClosingType.Normal);
        LightElementNode body = new LightElementNode("body", DisplayType.Block, ClosingType.Normal);
        document.AddChild(body);

        for (int i = 0; i < lines.Length; i++)
        {
            string line = lines[i];
            LightElementNode element;
            if (i == 0)
            {
                element = flyweight.GetElement("h1", DisplayType.Block, ClosingType.Normal);
                element.AddChild(new LightTextNode(line));
            }
            else if (line.Length < 20)
            {
                element = flyweight.GetElement("h2", DisplayType.Block, ClosingType.Normal);
                element.AddChild(new LightTextNode(line));
            }
            else if (char.IsWhiteSpace(line[0]))
            {
                element = flyweight.GetElement("blockquote", DisplayType.Block, ClosingType.Normal);
                element.AddChild(new LightTextNode(line));
            }
            else
            {
                element = flyweight.GetElement("p", DisplayType.Block, ClosingType.Normal);
                element.AddChild(new LightTextNode(line));
            }
            body.AddChild(element);
        }

        Console.WriteLine("OuterHTML:");
        Console.WriteLine(document.OuterHTML);
        Console.WriteLine("\nInnerHTML:");
        Console.WriteLine(document.InnerHTML);

        long memorySize = GC.GetTotalMemory(false);
        Console.WriteLine($"\nРозмір дерева в пам'яті: {memorySize} байт");
    }

    private static async Task<string[]> DownloadBookAsync(string url)
    {
        using (HttpClient client = new HttpClient())
        {
            string bookText = await client.GetStringAsync(url);
            return bookText.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        }
    }

    private static async Task LoadImageExample()
    {
        Console.WriteLine("Виберіть джерело завантаження зображення:\n1: Файлова система\n2: Мережа");
        int sourceChoice = int.Parse(Console.ReadLine());

        IImageLoaderStrategy loaderStrategy = sourceChoice switch
        {
            1 => new FileSystemImageLoader(),
            2 => new NetworkImageLoader(),
            _ => throw new InvalidOperationException("Невідоме джерело завантаження")
        };

        Console.WriteLine("Введіть шлях до зображення:");
        string path = Console.ReadLine();

        ImageElement imageElement = new ImageElement(loaderStrategy);
        string result = await imageElement.LoadImage(path);

        Console.WriteLine(result);
    }
}
