﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

// Basic class for read text
class SmartTextReader
{
    public char[,] ReadText(string filePath)
    {
        string[] lines = File.ReadAllLines(filePath);
        int maxLength = lines[0].Length;

        foreach (var line in lines)
        {
            if (line.Length > maxLength)
                maxLength = line.Length;
        }

        char[,] textArray = new char[lines.Length, maxLength];

        for (int i = 0; i < lines.Length; i++)
        {
            for (int j = 0; j < lines[i].Length; j++)
            {
                textArray[i, j] = lines[i][j];
            }
        }

        return textArray;
    }
}

// Proxy for SmartTextReader with login
class SmartTextChecker : SmartTextReader
{
    public new char[,] ReadText(string filePath)
    {
        Console.WriteLine($"Opening file: {filePath}");
        char[,] textArray = base.ReadText(filePath);
        Console.WriteLine($"Successfully read file: {filePath}");
        Console.WriteLine($"Total lines: {textArray.GetLength(0)}");
        Console.WriteLine($"Total characters: {textArray.Length}");
        Console.WriteLine($"Closing file: {filePath}");
        return textArray;
    }
}

// Proxy for SmartTextReader with restricted access
class SmartTextReaderLocker : SmartTextReader
{
    private Regex regex;

    public SmartTextReaderLocker(string pattern)
    {
        regex = new Regex(pattern);
    }

    public new char[,] ReadText(string filePath)
    {
        if (regex.IsMatch(filePath))
        {
            Console.WriteLine("Access denied!");
            return new char[0, 0];
        }
        else
        {
            return base.ReadText(filePath);
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        // Checked SmartTextChecker
        SmartTextChecker checker = new SmartTextChecker();
        char[,] textArray1 = checker.ReadText("text.txt");

        // Checked SmartTextReaderLocker
        SmartTextReaderLocker locker = new SmartTextReaderLocker(@"file\d\.txt");
        char[,] textArray2 = locker.ReadText("text.txt");
    }
}