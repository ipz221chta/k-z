﻿using System;

// Abstract class for shapes
abstract class Shape
{
    protected Renderer renderer;

    public Shape(Renderer renderer)
    {
        this.renderer = renderer;
    }

    public abstract void Draw();
}

// Specific classes of shapes
class Circle : Shape
{
    public Circle(Renderer renderer) : base(renderer)
    {
    }

    public override void Draw()
    {
        Console.Write("Drawing Circle ");
        renderer.Render();
    }
}

class Square : Shape
{
    public Square(Renderer renderer) : base(renderer)
    {
    }

    public override void Draw()
    {
        Console.Write("Drawing Square ");
        renderer.Render();
    }
}

class Triangle : Shape
{
    public Triangle(Renderer renderer) : base(renderer)
    {
    }

    public override void Draw()
    {
        Console.Write("Drawing Triangle ");
        renderer.Render();
    }
}

// An abstract class for the renderer
abstract class Renderer
{
    public abstract void Render();
}

// Specific renderer classes
class VectorRenderer : Renderer
{
    public override void Render()
    {
        Console.WriteLine("as vector graphic");
    }
}

class RasterRenderer : Renderer
{
    public override void Render()
    {
        Console.WriteLine("as pixels");
    }
}

class Program
{
    static void Main(string[] args)
    {
        Renderer vectorRenderer = new VectorRenderer();
        Renderer rasterRenderer = new RasterRenderer();

        Shape circle = new Circle(vectorRenderer);
        Shape square = new Square(rasterRenderer);
        Shape triangle = new Triangle(vectorRenderer);

        circle.Draw();
        square.Draw();
        triangle.Draw();
    }
}
