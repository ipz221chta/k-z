﻿using System;
using System.Collections.Generic;

// Inventory interface
interface IInventory
{
    void Equip();
}

// Basic class for hero
class Hero
{
    protected string name;
    protected List<IInventory> inventory;

    public Hero(string name)
    {
        this.name = name;
        inventory = new List<IInventory>();
    }

    public void AddInventoryItem(IInventory item)
    {
        inventory.Add(item);
    }

    public void EquipInventory()
    {
        Console.WriteLine($"{name}'s Inventory:");
        foreach (var item in inventory)
        {
            item.Equip();
        }
    }
}

// Class hero
class Warrior : Hero
{
    public Warrior(string name) : base(name)
    {
    }
}

class Mage : Hero
{
    public Mage(string name) : base(name)
    {
    }
}

class Paladin : Hero
{
    public Paladin(string name) : base(name)
    {
    }
}

// Decorators for inventory
class Weapon : IInventory
{
    private string type;

    public Weapon(string type)
    {
        this.type = type;
    }

    public void Equip()
    {
        Console.WriteLine($"Equipping {type} weapon");
    }
}

class Armor : IInventory
{
    private string type;

    public Armor(string type)
    {
        this.type = type;
    }

    public void Equip()
    {
        Console.WriteLine($"Equipping {type} armor");
    }
}

class Artifact : IInventory
{
    private string name;

    public Artifact(string name)
    {
        this.name = name;
    }

    public void Equip()
    {
        Console.WriteLine($"Equipping {name} artifact");
    }
}

class Program
{
    static void Main(string[] args)
    {
        Warrior warrior = new Warrior("Conan");
        Mage mage = new Mage("Gandalf");
        Paladin paladin = new Paladin("Arthur");

        warrior.AddInventoryItem(new Weapon("Sword"));
        warrior.AddInventoryItem(new Armor("Chainmail"));

        mage.AddInventoryItem(new Weapon("Staff"));
        mage.AddInventoryItem(new Armor("Robe"));
        mage.AddInventoryItem(new Artifact("Crystal Ball"));

        paladin.AddInventoryItem(new Weapon("Mace"));
        paladin.AddInventoryItem(new Armor("Plate Armor"));
        paladin.AddInventoryItem(new Artifact("Holy Grail"));

        warrior.EquipInventory();
        Console.WriteLine();

        mage.EquipInventory();
        Console.WriteLine();

        paladin.EquipInventory();
    }
}
