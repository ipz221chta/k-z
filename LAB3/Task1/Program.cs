﻿using System;
using System.IO;

public class Logger
{
    public void Log(string message)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine(message);
        Console.ResetColor();
    }

    public void Error(string message)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(message);
        Console.ResetColor();
    }

    public void Warn(string message)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine(message);
        Console.ResetColor();
    }
}


public class FileWriter
{
    private string filePath;

    public FileWriter(string filePath)
    {
        this.filePath = filePath;
    }

    public void Write(string message)
    {
        File.AppendAllText(filePath, message);
    }

    public void WriteLine(string message)
    {
        File.AppendAllText(filePath, message + Environment.NewLine);
    }
}
public class FileLoggerAdapter : Logger
{
    private FileWriter fileWriter;

    public FileLoggerAdapter(string filePath)
    {
        fileWriter = new FileWriter(filePath);
    }

    public new void Log(string message)
    {
        base.Log(message);
        fileWriter.WriteLine("Log: " + message);
    }

    public new void Error(string message)
    {
        base.Error(message);
        fileWriter.WriteLine("Error: " + message);
    }

    public new void Warn(string message)
    {
        base.Warn(message);
        fileWriter.WriteLine("Warn: " + message);
    }
}
class Program
{
    static void Main(string[] args)
    {
        Logger consoleLogger = new Logger();
        consoleLogger.Log("This is a log message.");
        consoleLogger.Error("This is an error message.");
        consoleLogger.Warn("This is a warning message.");

        FileLoggerAdapter fileLogger = new FileLoggerAdapter("log.txt");
        fileLogger.Log("This is a log message.");
        fileLogger.Error("This is an error message.");
        fileLogger.Warn("This is a warning message.");
    }
}
