﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.Intrinsics.X86;


//bacsic class for subscription
public abstract class Subscription
{
    public double MonthlyFee { get; protected set; }
    public int MinSubscriptionPeriod { get; protected set; }
    public List<string> Channels { get; protected set; }

    public Subscription(double monthlyFee, int minSubscriptionPeriod, List<string> channels)
    {
        MonthlyFee = monthlyFee;
        MinSubscriptionPeriod = minSubscriptionPeriod;
        Channels = channels;
    }

    public abstract void DisplayInfo();
}

//subscription for domestic
public class DomesticSubscription : Subscription
{
    public DomesticSubscription() : base(10.99, 1, new List<string> { "Local News", "Entertainment" })
    {
    }

    public override void DisplayInfo()
    {
        Console.WriteLine("Domestic Subscription:");
        Console.WriteLine($"Monthly Fee: ${MonthlyFee}");
        Console.WriteLine($"Minimum Subscription Period: {MinSubscriptionPeriod} month(s)");
        Console.WriteLine("Channels:");
        foreach (var channel in Channels)
        {
            Console.WriteLine($"- {channel}");
        }
    }
}

//education subscription
public class EducationalSubscription : Subscription
{
    public EducationalSubscription() : base(15.99, 3, new List<string> { "Documentaries", "Educational Channels" })
    {
    }

    public override void DisplayInfo()
    {
        Console.WriteLine("Educational Subscription:");
        Console.WriteLine($"Monthly Fee: ${MonthlyFee}");
        Console.WriteLine($"Minimum Subscription Period: {MinSubscriptionPeriod} month(s)");
        Console.WriteLine("Channels:");
        foreach (var channel in Channels)
        {
            Console.WriteLine($"- {channel}");
        }
    }
}

//premiun subscribion
public class PremiumSubscription : Subscription
{
    public PremiumSubscription() : base(25.99, 6, new List<string> { "All Channels", "No Ads" })
    {
    }

    public override void DisplayInfo()
    {
        Console.WriteLine("Premium Subscription:");
        Console.WriteLine($"Monthly Fee: ${MonthlyFee}");
        Console.WriteLine($"Minimum Subscription Period: {MinSubscriptionPeriod} month(s)");
        Console.WriteLine("Channels:");
        foreach (var channel in Channels)
        {
            Console.WriteLine($"- {channel}");
        }
    }
}

//fubrical class for creat subscription
public static class SubscriptionFactory
{
    public static Subscription PurchaseSubscription(string subscriptionType)
    {
        switch (subscriptionType.ToLower())
        {
            case "domestic":
                return new DomesticSubscription();
            case "educational":
                return new EducationalSubscription();
            case "premium":
                return new PremiumSubscription();
            default:
                throw new ArgumentException("Invalid subscription type.");
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        //Example of use

        // Purchase a subscription through the website
        Subscription websiteSubscription = SubscriptionFactory.PurchaseSubscription("premium");
        websiteSubscription.DisplayInfo();

        Console.WriteLine();

        // Purchase a subscription through the mobile application
        Subscription mobileAppSubscription = SubscriptionFactory.PurchaseSubscription("educational");
        mobileAppSubscription.DisplayInfo();

        Console.WriteLine();

        // Purchase a subscription by calling the manager
        Subscription managerCallSubscription = SubscriptionFactory.PurchaseSubscription("domestic");
        managerCallSubscription.DisplayInfo();
    }
}

