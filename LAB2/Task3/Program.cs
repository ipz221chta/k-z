﻿using System;

public sealed class Authenticator
{
    private static volatile Authenticator instance;
    private static readonly object syncRoot = new Object();

    // Private constructor to disallow external instantiation
    private Authenticator() { }

    // A method for obtaining a single instance of a class
    public static Authenticator GetInstance()
    {
        if (instance == null)
        {
            lock (syncRoot)
            {
                if (instance == null)
                    instance = new Authenticator();
            }
        }
        return instance;
    }

    // Additional methods for authentication
    public void AuthenticateUser(string username, string password)
    {
        // Implementation of user authentication
        Console.WriteLine($"Authenticating user {username} with password {password}...");
    }
}

class Program
{
    static void Main(string[] args)
    {
        //Attempting to create an instance via the GetInstance method
        Authenticator auth1 = Authenticator.GetInstance();
        Authenticator auth2 = Authenticator.GetInstance();

        // Checking that the received instances match
        Console.WriteLine($"auth1 equals auth2: {auth1 == auth2}");

        //Call the authentication method through any instance of the class
        auth1.AuthenticateUser("user1", "password123");
        auth2.AuthenticateUser("user2", "password456");
    }
}
