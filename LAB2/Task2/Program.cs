﻿using System;

// basic class for devidse
public abstract class Device
{
    public string Brand { get; protected set; }
    public string Type { get; protected set; }

    public Device(string brand, string type)
    {
        Brand = brand;
        Type = type;
    }

    public abstract void DisplayInfo();
}

// Classes for different types of equipment
public class Laptop : Device
{
    public Laptop(string brand) : base(brand, "Laptop") { }

    public override void DisplayInfo()
    {
        Console.WriteLine($"Brand: {Brand}, Type: {Type}");
    }
}

public class Netbook : Device
{
    public Netbook(string brand) : base(brand, "Netbook") { }

    public override void DisplayInfo()
    {
        Console.WriteLine($"Brand: {Brand}, Type: {Type}");
    }
}

public class EBook : Device
{
    public EBook(string brand) : base(brand, "EBook") { }

    public override void DisplayInfo()
    {
        Console.WriteLine($"Brand: {Brand}, Type: {Type}");
    }
}

public class Smartphone : Device
{
    public Smartphone(string brand) : base(brand, "Smartphone") { }

    public override void DisplayInfo()
    {
        Console.WriteLine($"Brand: {Brand}, Type: {Type}");
    }
}

// Equipment production factory
public class DeviceFactory
{
    public Device CreateDevice(string brand, string type)
    {
        switch (type.ToLower())
        {
            case "laptop":
                return new Laptop(brand);
            case "netbook":
                return new Netbook(brand);
            case "ebook":
                return new EBook(brand);
            case "smartphone":
                return new Smartphone(brand);
            default:
                throw new ArgumentException("Invalid device type.");
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        DeviceFactory factory = new DeviceFactory();

        Device laptop = factory.CreateDevice("IProne", "laptop");
        laptop.DisplayInfo();

        Device smartphone = factory.CreateDevice("Kiaomi", "smartphone");
        smartphone.DisplayInfo();
    }
}