﻿using System;
using System.Collections.Generic;

// Character builder interface
public interface ICharacterBuilder
{
    ICharacterBuilder SetHeight(double height);
    ICharacterBuilder SetBuild(string build);
    ICharacterBuilder SetHairColor(string hairColor);
    ICharacterBuilder SetEyeColor(string eyeColor);
    ICharacterBuilder SetClothing(string clothing);
    ICharacterBuilder SetInventory(List<string> inventory);
}

// A class for creating a hero
public class HeroBuilder : ICharacterBuilder
{
    private double _height;
    private string _build;
    private string _hairColor;
    private string _eyeColor;
    private string _clothing;
    private List<string> _inventory = new List<string>();

    public ICharacterBuilder SetHeight(double height)
    {
        _height = height;
        return this;
    }

    public ICharacterBuilder SetBuild(string build)
    {
        _build = build;
        return this;
    }

    public ICharacterBuilder SetHairColor(string hairColor)
    {
        _hairColor = hairColor;
        return this;
    }

    public ICharacterBuilder SetEyeColor(string eyeColor)
    {
        _eyeColor = eyeColor;
        return this;
    }

    public ICharacterBuilder SetClothing(string clothing)
    {
        _clothing = clothing;
        return this;
    }

    public ICharacterBuilder SetInventory(List<string> inventory)
    {
        _inventory = inventory;
        return this;
    }

    public Hero Build()
    {
        return new Hero(_height, _build, _hairColor, _eyeColor, _clothing, _inventory);
    }
}

// A class to create an enemy
public class EnemyBuilder : ICharacterBuilder
{
    private double _height;
    private string _build;
    private string _hairColor;
    private string _eyeColor;
    private string _clothing;
    private List<string> _inventory = new List<string>();

    public ICharacterBuilder SetHeight(double height)
    {
        _height = height;
        return this;
    }

    public ICharacterBuilder SetBuild(string build)
    {
        _build = build;
        return this;
    }

    public ICharacterBuilder SetHairColor(string hairColor)
    {
        _hairColor = hairColor;
        return this;
    }

    public ICharacterBuilder SetEyeColor(string eyeColor)
    {
        _eyeColor = eyeColor;
        return this;
    }

    public ICharacterBuilder SetClothing(string clothing)
    {
        _clothing = clothing;
        return this;
    }

    public ICharacterBuilder SetInventory(List<string> inventory)
    {
        _inventory = inventory;
        return this;
    }

    public Enemy Build()
    {
        return new Enemy(_height, _build, _hairColor, _eyeColor, _clothing, _inventory);
    }
}

// A class for the director of character creation
public class CharacterDirector
{
    private ICharacterBuilder _builder;

    public CharacterDirector(ICharacterBuilder builder)
    {
        _builder = builder;
    }

    public void ConstructCharacter(double height, string build, string hairColor, string eyeColor, string clothing, List<string> inventory)
    {
        _builder.SetHeight(height)
                .SetBuild(build)
                .SetHairColor(hairColor)
                .SetEyeColor(eyeColor)
                .SetClothing(clothing)
                .SetInventory(inventory);
    }
}

// class hero
public class Hero
{
    public double Height { get; set; }
    public string Build { get; set; }
    public string HairColor { get; set; }
    public string EyeColor { get; set; }
    public string Clothing { get; set; }
    public List<string> Inventory { get; set; }

    public Hero(double height, string build, string hairColor, string eyeColor, string clothing, List<string> inventory)
    {
        Height = height;
        Build = build;
        HairColor = hairColor;
        EyeColor = eyeColor;
        Clothing = clothing;
        Inventory = inventory;
    }
}

// class enemy
public class Enemy
{
    public double Height { get; set; }
    public string Build { get; set; }
    public string HairColor { get; set; }
    public string EyeColor { get; set; }
    public string Clothing { get; set; }
    public List<string> Inventory { get; set; }

    public Enemy(double height, string build, string hairColor, string eyeColor, string clothing, List<string> inventory)
    {
        Height = height;
        Build = build;
        HairColor = hairColor;
        EyeColor = eyeColor;
        Clothing = clothing;
        Inventory = inventory;
    }
}

class Program
{
    static void Main(string[] args)
    {
        // Creating a builder and director for the hero
        var heroBuilder = new HeroBuilder();
        var heroDirector = new CharacterDirector(heroBuilder);

        // Creating a hero
        heroDirector.ConstructCharacter(180, "Athletic", "Blonde", "Blue", "Armor", new List<string> { "Sword", "Shield" });
        var hero = heroBuilder.Build();

        // Derivation of the characteristics of the hero
        Console.WriteLine("Hero Created:");
        Console.WriteLine($"Height: {hero.Height}");
        Console.WriteLine($"Build: {hero.Build}");
        Console.WriteLine($"Hair Color: {hero.HairColor}");
        Console.WriteLine($"Eye Color: {hero.EyeColor}");
        Console.WriteLine($"Clothing: {hero.Clothing}");
        Console.WriteLine("Inventory:");
        foreach (var item in hero.Inventory)
        {
            Console.WriteLine($"- {item}");
        }
        Console.WriteLine();

        // Creating a builder and director for the enemy
        var enemyBuilder = new EnemyBuilder();
        var enemyDirector = new CharacterDirector(enemyBuilder);

        // Creating an enemy
        enemyDirector.ConstructCharacter(170, "Stocky", "Black", "Red", "Robe", new List<string> { "Dark Magic" });
        var enemy = enemyBuilder.Build();

        // Derivation of enemy characteristics
        Console.WriteLine("Enemy Created:");
        Console.WriteLine($"Height: {enemy.Height}");
        Console.WriteLine($"Build: {enemy.Build}");
        Console.WriteLine($"Hair Color: {enemy.HairColor}");
        Console.WriteLine($"Eye Color: {enemy.EyeColor}");
        Console.WriteLine($"Clothing: {enemy.Clothing}");
        Console.WriteLine("Inventory:");
        foreach (var item in enemy.Inventory)
        {
            Console.WriteLine($"- {item}");
        }
    }
}
