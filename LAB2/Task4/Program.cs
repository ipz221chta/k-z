﻿using System;
using System.Collections.Generic;
using System.Linq;


public class Virus : ICloneable
{
    public double Weight { get; set; }
    public int Age { get; set; }
    public string Name { get; set; }
    public string Species { get; set; }
    public List<Virus> Children { get; set; }

    public Virus(double weight, int age, string name, string species)
    {
        Weight = weight;
        Age = age;
        Name = name;
        Species = species;
        Children = new List<Virus>();
    }

    // Implementation of cloning with consideration of all children
    public object Clone()
    {
        Virus clone = new Virus(Weight, Age, Name, Species);
        foreach (var child in Children)
        {
            clone.Children.Add((Virus)child.Clone());
        }
        return clone;
    }

    // Output of information about the virus
    public void DisplayInfo(string indent = "")
    {
        Console.WriteLine($"{indent}Name: {Name}, Species: {Species}, Weight: {Weight}, Age: {Age}");
        foreach (var child in Children)
        {
            child.DisplayInfo(indent + "  ");
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        // creading virus
        Virus grandParentVirus = new Virus(0.1, 1, "Grandparent", "Coronavirus");
        Virus parentVirus1 = new Virus(0.2, 2, "Parent1", "Coronavirus");
        Virus parentVirus2 = new Virus(0.3, 2, "Parent2", "Coronavirus");
        Virus childVirus11 = new Virus(0.05, 1, "Child11", "Coronavirus");
        Virus childVirus12 = new Virus(0.06, 1, "Child12", "Coronavirus");
        Virus childVirus21 = new Virus(0.07, 1, "Child21", "Coronavirus");
        Virus childVirus22 = new Virus(0.08, 1, "Child22", "Coronavirus");

        // Formation of the hierarchy of viruses
        grandParentVirus.Children.Add(parentVirus1);
        grandParentVirus.Children.Add(parentVirus2);
        parentVirus1.Children.Add(childVirus11);
        parentVirus1.Children.Add(childVirus12);
        parentVirus2.Children.Add(childVirus21);
        parentVirus2.Children.Add(childVirus22);

        // Formation of the hierarchy of virusesviruses
        Virus clonedVirus = (Virus)grandParentVirus.Clone();

        // Output of information about the cloned virus
        Console.WriteLine("Cloned Virus Family:");
        clonedVirus.DisplayInfo();
    }
}

