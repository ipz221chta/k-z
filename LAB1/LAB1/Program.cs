﻿using System;
using System.Collections.Generic;
using System.Text;
public class Money
{
    public int WholePart { get; set; }
    public int Coints { get; set; }
    public Currency Currency { get; set; }
    public Money(int wholePart, int cents, Currency currency)
    {
        WholePart = wholePart;
        Coints = cents;
        Currency = currency;
    }

    // Метод для виведення суми
    public void DisplayAmount()
    {
        Console.WriteLine($"Сума: {WholePart}.{Coints:D2} {Currency.Symbol}");
    }
}

public class Product
{
    public string Name { get; set; }
    public int Unit { get; set; }
    public Money Price { get; set; }
    public Currency Currency { get; set; }

    // Метод для зменшення ціни продукту
    public void LowerPrice(int reductionAmount)
    {
        Price.WholePart -= reductionAmount;
        // Обробляє випадки, коли зменшення перевищує кількість копійок у ціні
        if (reductionAmount >= Price.Coints)
        {
            Price.WholePart -= 1;
            Price.Coints = 100 - (reductionAmount - Price.Coints);
        }
        else
        {
            Price.Coints -= reductionAmount;
        }
    }
}

public class Warehouse
{
    public string Name { get; set; }
    public string Unit { get; set; }
    public Money UnitPrice { get; set; }
    public int Quantity { get; set; }
    public DateTime LastRestockDate { get; set; }

    // Конструктор класу Warehouse
    public Warehouse(string name, string unit, Money unitPrice, int quantity, DateTime lastRestockDate)
    {
        Name = name;
        Unit = unit;
        UnitPrice = unitPrice;
        Quantity = quantity;
        LastRestockDate = lastRestockDate;
    }
}

public class Reporting
{
    // Метод для реєстрації надходження товару
    public void RegisterIncomingProduct(Warehouse warehouse, int quantity)
    {
        warehouse.Quantity += quantity;
        warehouse.LastRestockDate = DateTime.Now;
        Console.WriteLine($"Товар \"{warehouse.Name}\" надійшов на склад. Кількість: {quantity}");
    }

    // Метод для відвантаження товару
    public void ShipProduct(Warehouse warehouse, int quantity)
    {
        if (warehouse.Quantity >= quantity)
        {
            warehouse.Quantity -= quantity;
            Console.WriteLine($"Товар \"{warehouse.Name}\" відвантажено. Кількість: {quantity}");
        }
        else
        {
            Console.WriteLine($"Помилка: на складі недостатньо товару \"{warehouse.Name}\" для відвантаження.");
        }
    }

    // Метод для звіту по інвентаризації
    public void InventoryReport(Warehouse warehouse)
    {
        Console.WriteLine($"Інвентаризаційний звіт для товару \"{warehouse.Name}\":");
        Console.WriteLine($"Останній завоз: {warehouse.LastRestockDate}");
        Console.WriteLine($"Кількість на складі: {warehouse.Quantity}");
        Console.WriteLine($"Ціна за одиницю: {warehouse.UnitPrice.WholePart}.{warehouse.UnitPrice.Coints:D2}");
    }
}

//Дочірній клас валюти

public class Currency
{
    public string Code { get; set; }
    public string Symbol { get; set; }
}

public class UkrainianHryvnia : Currency
{
    public UkrainianHryvnia()
    {
        Code = "UAH";
        Symbol = "₴";
    }
}

public class USDCurrency : Currency
{
    public USDCurrency()
    {
        Code = "USD";
        Symbol = "$";
    }
}

//Додавання категорій для продуктів

public class ProductCategory
{
    public string Name { get; set; }
}

public class FoodCategory : ProductCategory
{
    public FoodCategory()
    {
        Name = "Food";
    }
}

public class ElectronicsCategory : ProductCategory
{
    public ElectronicsCategory()
    {
        Name = "Electronics";
    }
}

public class ClothingCategory : ProductCategory
{
    public ClothingCategory()
    {
        Name = "Clothing";
    }
}

public class HouseholdHoogsCategory : ProductCategory
{
    public HouseholdHoogsCategory()
    {
        Name = "House Goods";
    }
}

public class CosmeticCategory : ProductCategory
{
    public CosmeticCategory()
    {
        Name = "Cosmetics";
    }
}

// Корзина для замовлень

public class ShoppingCart
{
    public List<Product> Items { get; set; }

    public ShoppingCart()
    {
        Items = new List<Product>();
    }

    public void AddToCart(Product product, int quantity)
    {
        // Додавання продукту до корзини з вказаною кількістю
        // Перевірка наявності товару на складі
        if (product.Unit >= quantity)
        {
            Items.Add(product);
            Console.WriteLine($"Додано в корзину: {quantity} x {product.Name}");
        }
        else
        {
            Console.WriteLine($"Не можливо додати товар \"{product.Name}\" у корзину. Кількість одиниць на складі = {product.Unit}, ви хочете замовити {quantity}");
        }
    }

    public void DisplayCart()
    {
        Console.WriteLine("Замовлення в корзині:");
        foreach (var item in Items)
        {
            Console.WriteLine($"{item.Name} - {item.Price.WholePart}.{item.Price.Coints:D2} {item.Price.Currency.Symbol}");
        }
    }
}

class Program
{
    static void Main()
    {
        Console.OutputEncoding = Encoding.UTF8;

        // Створення екземплярів валют
        UkrainianHryvnia uahCurrency = new UkrainianHryvnia();
        USDCurrency usdCurrency = new USDCurrency();

        // Створення екземплярів продуктів
        Product laptop = new Product
        {
            Name = "Laptop",
            Unit = 10,
            Price = new Money(1500, 50, usdCurrency),
            Currency = usdCurrency
        };

        Product smartphone = new Product
        {
            Name = "Smartphone",
            Unit = 20,
            Price = new Money(800, 0, usdCurrency),
            Currency = usdCurrency
        };

        Product milk = new Product
        {
            Name = "Milk",
            Unit = 15,
            Price = new Money(25, 50, uahCurrency),
            Currency = uahCurrency
        };

        Product bread = new Product
        {
            Name = "Bread",
            Unit = 25,
            Price = new Money(15, 0, uahCurrency),
            Currency = uahCurrency
        };

        // Створення екземплярів категорій продуктів
        ElectronicsCategory electronicsCategory = new ElectronicsCategory();
        ClothingCategory clothingCategory = new ClothingCategory();
        FoodCategory foodCategory = new FoodCategory();

        // Додавання товарів на склад
        Warehouse electronicsWarehouse = new Warehouse("Electronics Warehouse", "pcs", laptop.Price, 50, DateTime.Now);
        Warehouse clothingWarehouse = new Warehouse("Clothing Warehouse", "pcs", new Money(30, 0, uahCurrency), 30, DateTime.Now);
        Warehouse foodWarehouse = new Warehouse("Food Warehouse", "pcs", milk.Price, 10, DateTime.Now);

        Reporting report = new Reporting();

        // Зареєструвати надходження товарів на склади
        report.RegisterIncomingProduct(electronicsWarehouse, laptop.Unit);
        report.RegisterIncomingProduct(clothingWarehouse, clothingWarehouse.Quantity);
        report.RegisterIncomingProduct(foodWarehouse, foodWarehouse.Quantity);

        // Зменшення ціни продуктів
        laptop.LowerPrice(100);
        smartphone.LowerPrice(50);
        milk.LowerPrice(5);
        bread.LowerPrice(3);

        // Створення екземпляра корзини для замовлень
        ShoppingCart cart = new ShoppingCart();

        // Додавання продуктів до корзини
        cart.AddToCart(laptop, 1);
        cart.AddToCart(smartphone, 3);
        cart.AddToCart(milk, 20);
        cart.AddToCart(bread, 8);

        // Виведення замовлення в корзині
        cart.DisplayCart();
    }
}